package com.xlrs.schedular;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import io.micronaut.context.exceptions.NoSuchMessageException;
import io.micronaut.json.tree.JsonObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.xlrs.schedular.exception.ApplicationException;
import com.xlrs.schedular.rest.IllionCustomerBankDataRestTemplate;
import com.xlrs.schedular.view.CustomerAccountView;
import com.xlrs.schedular.view.IllionUserLoginView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SchedularHandler  {


    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    public JsonObject handleRequest(Object input, Context context) throws NoSuchMessageException, ApplicationException {
        log.info("Cron Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));

        IllionCustomerBankDataRestTemplate illionCustomerBankDataRestTemplate = new IllionCustomerBankDataRestTemplate();

        JsonObject illionCustomerBankData = null;
        long startTime = System.currentTimeMillis();


        List<IllionUserLoginView> allLoginUsers = illionCustomerBankDataRestTemplate.getAllLoginUsers();
        for(IllionUserLoginView illionLogin: allLoginUsers) {
            try {
                log.debug("calling for customer : : : "+ illionLogin.toString());
                CustomerAccountView customerAccountView = new CustomerAccountView(illionLogin.getCustomerId(),illionLogin.getEncryptionKey(),illionLogin.getId());
                illionCustomerBankData = illionCustomerBankDataRestTemplate.insertCustomerBankData(customerAccountView);
                log.debug(illionCustomerBankData.toString());
                long endTime = System.currentTimeMillis();
                log.debug("That took " + ((endTime - startTime)/1000) + " seconds");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return illionCustomerBankData;
    }
}