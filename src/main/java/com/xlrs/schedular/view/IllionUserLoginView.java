package com.xlrs.schedular.view;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IllionUserLoginView implements Serializable{

    private static final long serialVersionUID = 8871751311199050454L;

    private Long id;
    private String customerId;
    private String encryptionKey;
    private String username;
    private String password;
    private String userRef;
}