package com.xlrs.schedular.view;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@ToString
public class CustomerAccountView implements Serializable{

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "{customerAccountView.customerId.mandatory.feild.notempty}")
    private String customerId;
    @NotEmpty(message = "{customerAccountView.encryptionKey.mandatory.feild.notempty}")
    private String encryptionKey;
    @NotEmpty(message = "{customerAccountView.orgUserId.mandatory.feild.notempty}")
    private Long orgUserId;

}
